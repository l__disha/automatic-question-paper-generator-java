/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author saibaba
 */
public class DatabaseWrapper {
    private static Connection conn;
    
    static {
	conn = DatabaseConnection.getConnection();
    }
    
    public static ResultSet getAll(String tableName) {
	String query = "SELECT * FROM " + tableName;
	ResultSet rs = executeQuery(query);
	return rs;
    }
    
    public static ResultSet executeQuery(String table, String conditions) {
	String query = "SELECT * FROM " + table + " WHERE " + conditions;
	ResultSet rs = executeQuery(query);
	return rs;
    }
    
    public static ResultSet executeQuery(String query) {
	try {
	    PreparedStatement ps = conn.prepareStatement(query);
	    ResultSet rs = ps.executeQuery();
	    return rs;
	} catch (SQLException e) {
	    JOptionPane.showMessageDialog(null, "Error while Fetching Records  " + e.getMessage());
	    return null;
	}
    }
}
