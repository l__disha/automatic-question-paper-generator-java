/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author saibaba
 */
public class DatabaseConnection {
    private static Connection conn;
    static {
	conn = getConnection();
    }
    
    public static Connection getConnection(){
	//will have to return modify if using multithreading
	if(conn != null) {
	    return conn;
	}
	try{
	    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/question_paper", "Disha", "Disha");
	    //JOptionPane.showMessageDialog(null, "Connection Established");
	    return conn;
	}catch(SQLException e){
	    JOptionPane.showMessageDialog(null, "Connection Failed " + e.getMessage());
	    return null;
	}
    }
}
