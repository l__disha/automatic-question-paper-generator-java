
package Files;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;
import model.MainQuestion;
import model.Question;
import model.QuestionPaper;

/**
 *
 * @author saibaba
 */
public class Files {
    
//    op in pdf form
    public static void printQuestionPaper(QuestionPaper qp) {
	try{
	    Document document = new Document();
	    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("test.pdf"));
	    
	    document.open();
	    
	    ArrayList<MainQuestion> mainQuestions = qp.getMainQuestions();
	    Vector<Question> questions = null;
	    int totalMainQuestions = mainQuestions.size();
	    int i=0;
	    MainQuestion mainQuestion;
	    Question question;
//	    Paragraph newParagraph;
	    
	    Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 14, BaseColor.BLACK);
//	    Chunk chunk;
	    List mainQuestionList = new List(List.ORDERED);
	    ListItem listItem;
	    while(i < totalMainQuestions) {
		System.out.println("totalMainQuestions: " + totalMainQuestions);
		mainQuestion = mainQuestions.get(i);
		
		String mainQuestionString = mainQuestion.getBody();

//		chunk = new Chunk(mainQuestionString, font);
//		newParagraph = new Paragraph(mainQuestionString, font);
//		document.add(newParagraph);
		
		listItem = new ListItem(mainQuestionString, font);

		questions = mainQuestion.getQuestions();
		
		List subQuestionPdfList = new List(List.ORDERED);

		int totalSubQuestions, index=0;
		totalSubQuestions = questions.size();

		while(index < totalSubQuestions) {
		    question = questions.get(index);

		    String questionBody = question.getQuestionBody();
		    
		    subQuestionPdfList.add(new ListItem(questionBody));

		    index++;
		}
		
		listItem.add(subQuestionPdfList);
		listItem.add(Chunk.NEWLINE);
		mainQuestionList.add(listItem);
		
//		mainQuestionList.add(subQuestionPdfList);
		i++;
	    }
	    document.add(mainQuestionList);
	    document.close();
	    writer.close();
	} catch (FileNotFoundException ie) {
	    System.out.println("FileNotFoundException caught! " + ie.getMessage());
	} catch (DocumentException ie) {
	    System.out.println("DocumentException caught! " + ie.getMessage());
	}
    
    }
  
//    public static void printQuestionPaper(QuestionPaper qp) {    
//	String questionString = getFileContentsFromQuestionPaper(qp);
//	try {
//	    Document document = new Document();
//	    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("test.pdf"));
//	    document.open();
//	    document.add(new Paragraph(questionString));
//	    document.add(new Paragraph(questionString));
//	    document.close();
//	    writer.close();
//	} catch (FileNotFoundException ie) {
//	    System.out.println("FileNotFoundException caught! " + ie.getMessage());
//	}  catch (DocumentException e){
//	    System.out.println("DocumentException caught! " + e.getMessage());
//	}
//    }
//    
    
    private static String getFileContentsFromQuestionPaper(QuestionPaper qp){
	ArrayList<MainQuestion> mainQuestions = qp.getMainQuestions();
	Vector<Question> questions = null;
	int totalMainQuestions = mainQuestions.size();
	int i=0;
	String mainString = "";
	MainQuestion mainQuestion;
	while(i < totalMainQuestions) {
	    System.out.println("totalMainQuestions: " + totalMainQuestions);
	    mainQuestion = mainQuestions.get(i);
	    mainString = mainString + "\n" + mainQuestion.getQuestionNo() + " " + mainQuestion.getBody();
	    questions = mainQuestion.getQuestions();
	    
	    int totalSubQuestions, index=0;
	    totalSubQuestions = questions.size();
	    String subQuestion = "";
	    
	    while(index < totalSubQuestions) {
		String questionBody = questions.get(index).getQuestionBody();
		subQuestion = subQuestion + "\n" + questionBody;
		
		index++;
	    }
	    
	    mainString = mainString + subQuestion + "\n\n";
	    i++;
	}
	
	return mainString;
    }
    
        //op in txt form
//    public static void printQuestionPaper(QuestionPaper qp) {
////	String questionString = getFileContentsFromQuestionPaper(qp);
//	try(PrintWriter writer = new PrintWriter(new FileWriter("test.txt", true));) {
////	    writer.write(questionString);
//
//	    writer.write("");
//	    List<MainQuestion> mainQuestions = qp.getMainQuestions();
//	    Vector<Question> questions = null;
//	    int totalMainQuestions = mainQuestions.size();
//	    int i=0;
//	    MainQuestion mainQuestion;
//	    Question question;
//	    while(i < totalMainQuestions) {
//		System.out.println("totalMainQuestions: " + totalMainQuestions);
//		mainQuestion = mainQuestions.get(i);
//		writer.println();
//		
//		String mainQuestionString = mainQuestion.getQuestionNo() + " " + mainQuestion.getBody();
//		writer.append(mainQuestionString);
//		writer.println();
//		questions = mainQuestion.getQuestions();
//
//		int totalSubQuestions, index=0;
//		totalSubQuestions = questions.size();
//		int position =  1;
//		while(index < totalSubQuestions) {
//		    question = questions.get(index);
//		    position = index + 1;
//		    String questionBody = position + " " + question.getQuestionBody();
//		    writer.append(questionBody);
//		    writer.println();
//
//		    index++;
//		}
//
//		writer.println();
//		i++;
//	    }
//	    
//	    writer.close();
//	} catch (IOException ie) {
//	    System.out.println("IOException caught! " + ie.getMessage());
//	}
//    
//    }

}
