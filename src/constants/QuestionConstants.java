/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package constants;

/**
 *
 * @author saibaba
 */
public interface QuestionConstants {
    String table = "questions";
    String COL_ID = "id";
    String COL_QUESTION_TITLE = "question_body";
    String COL_DIFFICULTY_LEVEL = "difficulty_level";
    String COL_MARKS = "min_marks";
    String COL_CHAPTER_ID = "chapter_id";
}
