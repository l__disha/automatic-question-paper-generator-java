/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package constants;

/**
 *
 * @author saibaba
 */
public interface ChapterConstants {
    String TABLE = "chapters";
    String COL_ID = "id";
    String COL_NAME = "name";
    String COL_SUBJECT_ID = "subject_id";
}
