/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import Files.Files;
import java.util.Random;
import model.QuestionPaper;


public class AutomaticQuestionPaper{

    private static final Random random = new Random();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {	//System.out.println("Select the subject: ");
	printQuestionPaper();
    }
    
    private static void printQuestionPaper() {
	int subjectId = 30;
	System.out.println("Subject Selected: " + subjectId);
	
	int difficultyLevel = Constants.DIFFICULTY_LEVEL_MEDIUM;
	System.out.println("Enter the difficulty Level: " + difficultyLevel);
	
	int marksPerSubquestion = 4;
	int noOfSubquestions = 5;
	int formatId  = 1;
	generateQuestionPaper(formatId, subjectId, difficultyLevel);
    }

    
    private static void generateQuestionPaper(int questionPaperFormatId, int subjectId, int difficultyLevel) {
//	QuestionPaper questionPaper = QuestionPaper.getQuestionPaper(questionPaperFormatId, subjectId, difficultyLevel);
	
	QuestionPaper questionPaper = new QuestionPaper(questionPaperFormatId, subjectId, difficultyLevel);
	
	System.out.println("Trying files!!");
	
	Files.printQuestionPaper(questionPaper);
    }
    
 /*   
    private static Vector<Question> generateQuestionsForMainQuestion(int subjectId, int difficultyLevel, int marksPerQuestion, int noOfQuestions) {
	Vector<Question> allQuestions = Question.getAllQuestionsOfSubject(subjectId, marksPerQuestion);
	Vector<Question> questions = getRandomQuestionsFrom(allQuestions, noOfQuestions);
	
	return questions;
    }

    private static Vector<Question> getRandomQuestionsFrom(Vector<Question> allQuestions, int noOfQuestions) {
	if(allQuestions == null){
	    System.out.println("No Questions Present ");
	    return null;
	}
	
	Vector<Question> selectedQuestions = new Vector<>();
	int totalQuestions = allQuestions.size();
	System.out.println("No of Questions: " +  totalQuestions);
	
	while(totalQuestions > 0 && noOfQuestions > 0) {
	    //select random question:
	    int currentSelectedQuestionIndex = random.nextInt(totalQuestions);
	    Question selectedQuestion = allQuestions.elementAt(currentSelectedQuestionIndex);
	    selectedQuestions.add(selectedQuestion);
	    System.out.println("selected Question id: " + selectedQuestion.getId());
	    allQuestions.remove(currentSelectedQuestionIndex);
	    totalQuestions--;
	    noOfQuestions--;
	}
	if(totalQuestions == 0 && noOfQuestions < 0) {
	    System.out.println("No qustions to be added");
	}

	System.out.println(selectedQuestions);
	return selectedQuestions;
    }
  */  
    /*
    private static void generateQuestionPaper(int subjectId, int difficultyLevel, int totalMarks) {
	//add weightage acc to chapters as well
	//ask for noOfMainQuestions
	//details of each mainQuestion

	Vector<Question> allQuestions =  Question.getAllQuestionsOfSubject(subjectId);
	if(allQuestions == null){
	    System.out.println("No Questions Present ");
	    return;
	}

	System.out.println("No of Questions: " +  allQuestions.size());
	Vector<Question> selectedQuestions = new Vector<>();
	int totalQuestions = allQuestions.size();
	LinkedList<Integer> question_ids_in_paper = new LinkedList<>();
	int remainingMarksQuestionsToBeAdded = totalMarks;
	int trial = 0;
	while(totalQuestions > 0 && remainingMarksQuestionsToBeAdded > 0) {
	    //select random question:
	    int currentSelectedQuestionIndex = random.nextInt(totalQuestions);
	    Question selectedQuestion = allQuestions.elementAt(currentSelectedQuestionIndex);
	    int selectedQuestionMarks = selectedQuestion.getMinMarks();
	    if(selectedQuestionMarks < remainingMarksQuestionsToBeAdded) {
		selectedQuestions.add(selectedQuestion);
		remainingMarksQuestionsToBeAdded = remainingMarksQuestionsToBeAdded - selectedQuestionMarks;

		System.out.println("adding Question: " + selectedQuestion.getId());
		System.out.println("Remaining marks " + remainingMarksQuestionsToBeAdded);
	    }else if(remainingMarksQuestionsToBeAdded < 3 ) {
		if(trial > 3){
		    break;
		}
		System.out.println("Trial");
		trial++;
	    }
	    allQuestions.remove(currentSelectedQuestionIndex);
	    totalQuestions--;
	}

	System.out.println(selectedQuestions);
    }
    */
}
