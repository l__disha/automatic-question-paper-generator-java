/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import constants.SubjectConstants;
import database.DatabaseConnection;
import database.DatabaseWrapper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author saibaba
 */
public class Subject implements SubjectConstants {
    private static Vector<Subject> allSubjects;
    private String name;
    private int id;
    private static Random random;
    
    static {
	allSubjects = new Vector<>();
	random = new Random();
    }
    
    Subject (int id, String name) {
	this.id = id;
	this.name = name;
    }
    /**
     * public static ArrayList<Subject> getAllSubjects and then use it in print function
     */
    private static void fetchAllSubjects() {
//	later
//	if(allSubjects.length != noOrecords) {
//	    update allSubjects
//	}
	    ResultSet rs = DatabaseWrapper.getAll("subjects");
	    allSubjects = getSubjectsFromResultSet(rs);
    }
    
    public static Subject getRandomSubject() {
	fetchAllSubjects();
	int totalSubjects = allSubjects.size();
	if(totalSubjects > 0){
	    int randomIndex = random.nextInt(totalSubjects);
	    return allSubjects.get(randomIndex);
	}
	return null;
    }
    
    public static Subject getSubjectUsingId(int id) {
	String condition = COL_ID + " = " + id;
	ResultSet rs = DatabaseWrapper.executeQuery(TABLE, condition);
	Vector<Subject> subjectList = getSubjectsFromResultSet(rs);
	if(subjectList != null && subjectList.size() > 0) {
	    Subject subject = subjectList.get(0);
	    return subject;
	}
	return null;
    }
    
    private static Vector<Subject> getSubjectsFromResultSet(ResultSet rs) {
	if(rs == null) {
	    return null;
	}
	
	Vector<Subject> subjectList = new Vector<>();
	try {
	    while ( rs.next() ) {
		subjectList.add(
		    new Subject (
			Integer.parseInt(rs.getString("id")),
			rs.getString("name")
		    )
		);
	    }
	} catch (SQLException e) {
	    JOptionPane.showMessageDialog(null, "Error while Fetching subjects " + e.getMessage());
	}
	
	return subjectList;
    }
    
    public static void printAllSubjects() {
	
//	if(allSubjects.isEmpty()) {
	   fetchAllSubjects();
//	}
	
	allSubjects.forEach( (subject) -> {
	    System.out.println(subject.getName());
	});
    }
    
    public String getName() {
	return this.name;
    }
    
    public int getId() {
	return this.id;
    }
}
