/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.Random;
import java.util.Vector;

public class MainQuestion {
    private final int questionNumber;
    private final String body;
    private final int marksPerQuestion;
    private final int noOfSubQuestions;
    private Vector<Question> questionList;
//    private Vector<Question> allQuestions;
    
    MainQuestion(int questionNumber, String body, int marksPerQuestion, int noOfSubQuestions) {
	this.body = body;
	this.marksPerQuestion = marksPerQuestion;
	this.noOfSubQuestions = noOfSubQuestions;
	this.questionNumber = questionNumber;
	this.questionList = null;
    }
    
    public void generateQuestions(int subjectId, int difficultyLevel) {
	Vector<Question> allQuestions = Question.getAllQuestionsOfSubject(subjectId, marksPerQuestion);
	Vector<Question> questions = getRandomQuestionsFrom(allQuestions, noOfSubQuestions);
	
	this.questionList = questions;
    }
    
    
    private Vector<Question> getRandomQuestionsFrom(Vector<Question> allQuestions, int noOfQuestions) {
	Random random = new Random();
	if(allQuestions == null){
	    System.out.println("No Questions Present ");
	    return null;
	}
	
	Vector<Question> selectedQuestions = new Vector<>();
	int totalQuestions = allQuestions.size();
	System.out.println("No of Questions: " +  totalQuestions);
	
	while(totalQuestions > 0 && noOfQuestions > 0) {
	    //select random question:
	    int currentSelectedQuestionIndex = random.nextInt(totalQuestions);
	    Question selectedQuestion = allQuestions.elementAt(currentSelectedQuestionIndex);
	    selectedQuestions.add(selectedQuestion);
	    System.out.println("selected Question id: " + selectedQuestion.getId());
	    allQuestions.remove(currentSelectedQuestionIndex);
	    totalQuestions--;
	    noOfQuestions--;
	}
	if(totalQuestions == 0 && noOfQuestions < 0) {
	    System.out.println("No qustions to be added");
	}

	System.out.println(selectedQuestions);
	return selectedQuestions;
    }
    
    public int getQuestionNo() {
	return this.questionNumber;
    }
    
    public String getBody() {
	return this.body;
    }
    
    public Vector<Question> getQuestions() {
	return new Vector(this.questionList);
    }
}
