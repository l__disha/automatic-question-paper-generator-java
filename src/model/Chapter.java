/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import database.DatabaseWrapper;
import java.sql.ResultSet;
import java.util.Vector;
import constants.ChapterConstants;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author saibaba
 */
public class Chapter implements ChapterConstants{
    private int id;
    private String name;
    private int subjectId;
    
    private Chapter (int id, String name, int subjectId) {
	this.id = id;
	this.name = name;
	this.subjectId = subjectId;
    }
    
    public static Vector<Chapter> getAllChapters(Subject subject) {
	String condition = "subject_id = " + subject.getId(); 
	ResultSet rs = DatabaseWrapper.executeQuery(TABLE, condition);
	Vector<Chapter> chapterList = getChaptersFromResultSet(rs);
	return chapterList;
    }
    
    public static String getIdsOfChapters(List<Chapter> chaptersToBeIncluded) {
	String chapterIds = "";
	//create a string containing ids of all chaptersToBeIncluded comma seperated!
	int noOfChapters = chaptersToBeIncluded.size();
	for(int i=0; i<noOfChapters-1; ++i ) {
	    chapterIds = chapterIds + chaptersToBeIncluded.get(i).getId() + ", ";
	}
	chapterIds = chapterIds + chaptersToBeIncluded.get(noOfChapters-1).getId();
	return chapterIds;
    }
    
    private static Vector<Chapter> getChaptersFromResultSet(ResultSet rs) {
	if(rs == null) {
	    return null;
	}
	
	Vector<Chapter> chapterList = new Vector<>();
	int count = 0;
	try {
	    while(rs.next()) {
		count++;
		chapterList.add(
		    new Chapter(
			rs.getInt(COL_ID),
			rs.getString(COL_NAME),
			rs.getInt(COL_SUBJECT_ID)
		    )
		);
	    }
	    System.out.println("Count: "+  count);
	} catch(SQLException e) {
	    JOptionPane.showMessageDialog(null, "Error while Fetching Records  " + e.getMessage());
	    return null;
	}
	
	return chapterList;
    }

    public int getId() {
	return this.id;
    }
    
    public String getName() {
	return this.name;
    }
    
    public int getSubjectId() {
	return this.subjectId;
    }
}
