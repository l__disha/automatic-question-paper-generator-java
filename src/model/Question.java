/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import constants.QuestionConstants;
import database.DatabaseWrapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author saibaba
 */
public class Question implements QuestionConstants{
    
    private String title;
    private int id;
    private int difficulty_level;
    private int marks;
    
    Question(int id, String title, int difficulty_level, int marks) {
	this.id = id;
	this.title = title;
	this.difficulty_level = difficulty_level;
	this.marks = marks;
    }
    
//    public static Vector<Question> getAllQuestions(Subject subject) {
//	//String sql_query = "SELECT * FROM questions WHERE subject_id = " + subject.getId();
//	String condition= "subject_id = " + subject.getId();
//	ResultSet rs = DatabaseWrapper.executeQuery(table, condition);
//	Vector<Question> questions_list = getQuestionsFromResultSet(rs);
//	return questions_list;
//    }
/*
    
    "SELECT * FROM questions WHERE chapter_id IN (SELECT chapters.id FROM chapters WHERE subject_id IN (SELECT id as subject_id FROM subjects WHERE name = ?))"
    */
    public static Vector<Question> getAllQuestionsOfSubject(int subject_id) {
	//String sql_query = "SELECT * FROM questions WHERE subject_id = " + subject.getId();
	//String condition= "subject_id = " + subject.getId();
	//ResultSet rs = DatabaseWrapper.executeQuery(table, condition);
	
	Vector<Chapter> chaptersToBeIncluded = new Vector<>();
	//chaptersToBeIncluded = Chapter.getAllChapters(subject);
	
	
	String query = "SELECT * FROM questions WHERE chapter_id IN (SELECT chapters.id FROM chapters WHERE subject_id = " + subject_id + " )";
	Vector<Question> allQuestions = getQuestionsFromQuery(query);
	return allQuestions;
    }
    
        public static Vector<Question> getAllQuestionsOfSubject(int subject_id, int marks) {
	//String sql_query = "SELECT * FROM questions WHERE subject_id = " + subject.getId();
	//String condition= "subject_id = " + subject.getId();
	//ResultSet rs = DatabaseWrapper.executeQuery(table, condition);
	
	Vector<Chapter> chaptersToBeIncluded = new Vector<>();
	//chaptersToBeIncluded = Chapter.getAllChapters(subject);
	
	
	String query = "SELECT * FROM questions WHERE chapter_id IN (SELECT chapters.id FROM chapters WHERE subject_id = " + subject_id + " ) AND min_marks <= " + marks + " AND max_marks <= " + marks;
	
	return getQuestionsFromQuery(query);
    }
    
//    public static Vector<Question> getAllQuestions(List<Chapter> chaptersToBeIncluded) {
//	//String sql_query = "SELECT * FROM questions WHERE subject_id = " + subject.getId();
//	ResultSet rs = DatabaseWrapper.executeQuery(table, condition);
//	Vector<Question> allQuestions = getQuestionsFromResultSet(rs);
//	return allQuestions;
//    }
	
	private static Vector<Question> getQuestionsFromQuery(String query) {
	    ResultSet rs = DatabaseWrapper.executeQuery(query);
	    Vector<Question> allQuestions = getQuestionsFromResultSet(rs);
	    return allQuestions;
	}
    
    public static Vector<Question> getAllQuestions(List<Chapter> chaptersToBeIncluded) {
	//String sql_query = "SELECT * FROM questions WHERE subject_id = " + subject.getId();
	String chapterIds = Chapter.getIdsOfChapters(chaptersToBeIncluded);

	String condition = COL_CHAPTER_ID + " IN (" + chapterIds + ")";
	System.out.println(condition);
	ResultSet rs = DatabaseWrapper.executeQuery(table, condition);
	Vector<Question> allQuestions = getQuestionsFromResultSet(rs);
	return allQuestions;
    }
    
    private static Vector<Question> getQuestionsFromResultSet(ResultSet rs) {
	Vector<Question> question_list = new Vector<>();
	if (rs != null) {
	    try {
		while(rs.next()) {
//		    switch() {
//			
//		    }
		    question_list.add(
			new Question (
			    rs.getInt(COL_ID),
			    rs.getString(COL_QUESTION_TITLE),
			    rs.getInt(COL_DIFFICULTY_LEVEL),
			    rs.getInt(COL_MARKS)
			)
		    );
		}
		return question_list;
	    } catch(SQLException e) {
			JOptionPane.showMessageDialog(null, "Error while Fetching Records  " + e.getMessage());
		return null;
	    }
	}
	//if rs is null
	return null;
    }
    
    public int getMinMarks() {
	return this.marks;
    }
    
    public int getId() {
	return this.id;
    }
    
    public String getQuestionBody() {
	return this.title;
    }
}
