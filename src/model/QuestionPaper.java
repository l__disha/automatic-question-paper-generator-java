/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import database.DatabaseWrapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author saibaba
 */
public class QuestionPaper  {
    ArrayList<MainQuestion> mainQuestions;
    int subjectId;
    int difficultyLevel;
    int questionPaperFormatId;
    public QuestionPaper(int questionPaperFormatId, int subjectId, int difficultyLevel) {
	this.questionPaperFormatId = questionPaperFormatId;
	this.subjectId = subjectId;
	this.difficultyLevel = difficultyLevel;
	initializeMainQuestions();
    }
    
//    public static QuestionPaper getQuestionPaper(int questionPaperFormatId, int subjectId, int difficultyLevel) {
//	QuestionPaper questionPaper = new QuestionPaper(questionPaperFormatId, subjectId, difficultyLevel);
//	
//	return questionPaper;
//    }
    
    private void initializeMainQuestions() {
	
	String sqlQuery = "SELECT * FROM main_questions WHERE format_id = " + questionPaperFormatId;
	ResultSet rs =  DatabaseWrapper.executeQuery(sqlQuery);
	
	int noOfMainQuestions = 0;
	this.mainQuestions = new ArrayList<>();
	System.out.println("Generating mainQuestions for format: " + questionPaperFormatId);
	if(rs != null ) {
	    MainQuestion mainQuestion;
	    try {
		while(rs.next()) {
		    noOfMainQuestions++;
		    System.out.println("Generating mainQuestions: " + noOfMainQuestions);
		    mainQuestion = new MainQuestion(
			noOfMainQuestions, 
			rs.getString("question_title"),
			rs.getInt("marks_per_sub_question"),
			rs.getInt("total_sub_questions")
		    );
		    mainQuestion.generateQuestions(subjectId, difficultyLevel);
		    this.mainQuestions.add(
			mainQuestion
		    );
		}
	    } catch (SQLException e) {
		JOptionPane.showMessageDialog(null, "Error while Fetching Records  " + e.getMessage());
	    }
	    
	}
    }
 
    public ArrayList<MainQuestion> getMainQuestions() {
	return new ArrayList<>(this.mainQuestions);
    }
}
